package nl.renedegroot.android.surveywidget

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import nl.renedegroot.android.survey.SurveyFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                add(R.id.first, SurveyFragment(), "survey")
            }
        }
    }
}

package nl.renedegroot.android.survey.internal

import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension

@Preview
@Composable
fun SampleSurvey() {
    Survey(
        title = "Some title", listOf(
            "first" to 0.3f,
            "second" to 0.5f,
            "third" to 0.1f,
            "forth" to 0.2f,
        )
    )
}

@Composable
fun Survey(title: String, values: List<Pair<String, Float>>) {
    ConstraintLayout(
        modifier = Modifier
            .wrapContentHeight()
            .fillMaxWidth()
    ) {
        val (header, labels, bars) = createRefs()
        Text(
            text = title,
            modifier = Modifier.constrainAs(header) {
                top.linkTo(parent.top, margin = 16.dp)
                start.linkTo(parent.start, margin = 16.dp)
            },
        )
        Column(
            modifier = Modifier.constrainAs(labels) {
                top.linkTo(header.bottom, margin = 8.dp)
                start.linkTo(parent.start, margin = 16.dp)
                bottom.linkTo(parent.bottom, margin = 16.dp)
            },
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            for (value in values) {
                Text(text = value.first)
            }
        }
        Column(
            modifier = Modifier.constrainAs(bars) {
                top.linkTo(labels.top)
                start.linkTo(labels.end, margin = 0.dp)
                end.linkTo(parent.end)
                bottom.linkTo(parent.bottom, margin = 16.dp)
                height = Dimension.fillToConstraints
                width = Dimension.fillToConstraints
            },
        ) {
            for (value in values) {
                Box(
                    modifier = Modifier.weight(1f),
                    contentAlignment = Alignment.Center,
                ) {
                    val barWidth: Float by animateFloatAsState(
                        targetValue = value.second,
                        animationSpec = tween(durationMillis = 300, easing = FastOutSlowInEasing)
                    )
                    Surface(
                        modifier = Modifier
                            .height(10.dp)
                            .fillMaxWidth(fraction = barWidth)
                            .padding(start = 4.dp),
                        color = Color.Red
                    ) { }
                }
            }
        }
    }
}
